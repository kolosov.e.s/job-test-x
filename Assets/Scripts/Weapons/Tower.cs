﻿using System.Collections;
using UnityEngine;
using System.Linq;

public abstract class Tower : MonoBehaviour {
    //Скорострельность
    public float m_shootInterval = 0.5f;
    //Дальнобойность
    public float m_range = 4f;
    //Снаряд
    public GameObject m_projectilePrefab;

    //Захваченная цель
    protected GameObject target;
    //Разрешено ли стрелять - реализация задержки
    protected bool allowShot = true;
    
    //При старте приложения отдельно запускаем поиск и отдельно стрельбу
    protected virtual void Start() {
        if (InitCheck()) { 
            StartCoroutine(FindTarget());
            StartCoroutine(ShotCheck());
        }
    }

    protected virtual IEnumerator FindTarget() {
        while (true) {
            
            target = Spawner.instance.livingMonsters.FirstOrDefault(m => TargetCheck(m));

            yield return null;
        }
    }
    protected virtual IEnumerator ShotCheck() {
        while (true) {
            if (TargetCheck(target) && allowShot) {
                allowShot = false;
                Shot(target);
                yield return new WaitForSeconds(m_shootInterval);
                allowShot = true;
            }
            else yield return null;
        }
    }

    //Чек инициализации
    protected virtual bool InitCheck() {
        return m_projectilePrefab != null;
    }
    //Чек цели
    protected virtual bool TargetCheck(GameObject checkedTarget) {
        return checkedTarget != null && (checkedTarget.transform.position - transform.position).sqrMagnitude < (m_range * m_range);
    }
    //Выстрел - у каждой пушки свой, поэтому реализацию оставляем на конкретный класс
    protected abstract void Shot(GameObject target);
}
