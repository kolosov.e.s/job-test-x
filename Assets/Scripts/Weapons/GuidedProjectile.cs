﻿using UnityEngine;

public class GuidedProjectile : Projectile
{
	public GameObject m_target;
    //Переопределяем полёт снаряда за целью
    protected override void Update () {
		if (m_target == null) {
			Destroy (gameObject);
			return;
		}
        //Переопределяем стандартное движение снаряда по прямой на движение за целью
		var translation = m_target.transform.position - transform.position;         //Направление перемещения к цели
		translation = translation.normalized * m_speed * Time.deltaTime;            //Применяем скорость и deltaTime
        transform.position += translation;                                          //Перемещаем к цели
    }
}
