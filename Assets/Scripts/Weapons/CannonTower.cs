﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class CannonTower : Tower
{
    //Скорость поворота башни
    public float m_rotationSpeed = 5f;
    //Точка создания снаряда
	public Transform m_shootPoint;
    //Пушка (вращение по X)
    public Transform m_cannon;

    
    //Предыдущее положение цели
    private Vector3 previousTargetPosition;
    //Положение цели
    private Vector3 targetPosition;
    //Скорость цели
    private Vector3 targetSpeed;
    //Разрешено ли поворачитвать - в момент стрельбы пушка не должна поворачиваться некоторое время (отдача, перезарядка, прочее)
    private bool allowRotate = true;
    float m_shootPointY;

    Quaternion rotateQuaternion;

    //Переопределили чек инициалиции за счет доп. объекта
    protected override bool InitCheck() {
        return m_projectilePrefab != null && m_shootPoint != null && m_cannon !=null;
    }

    //Переопределяем выстрел - ориентируем снаряд по стволу
    protected override void Shot(GameObject target) {
        //Снаряд летит из точки выстрела в направлении ствола
        Instantiate(m_projectilePrefab, m_shootPoint.position, Quaternion.FromToRotation(m_projectilePrefab.transform.forward, m_cannon.forward));
    }

    //Переопределяем чек выстрела за счет сверки планируемой линии стрельбы и линии пушки, а также запрещения поворота
    protected override IEnumerator ShotCheck()
    {
        while (true)
        {            
            if (TargetCheck(target) && allowShot && rotateQuaternion == transform.rotation)
            {
                allowRotate = false;
                allowShot = false;
                Shot(target);
                StartCoroutine(AllowRotate());
                yield return new WaitForSeconds(m_shootInterval);
                allowShot = true;
            }
            else yield return null;
        }
    }

    //Переопределяем старт - добавляем вращение
    protected override void Start()
    {
        if (InitCheck())
        {
            m_shootPointY = m_shootPoint.localPosition.y;
            StartCoroutine(FindTarget());
            StartCoroutine(ShotCheck());
            StartCoroutine(Rotate());
        }
    }

    void Update()
    {
        //Текущее положение цели
        if (target != null)
            previousTargetPosition = target.transform.position;        
    }
   
    void LateUpdate()
    {
        //В конце каждого кадра высчитываем скорость цели на основании времени и пройденного расстояния
        if (target != null)            
            targetSpeed = (target.transform.position - previousTargetPosition) / Time.deltaTime; // V=S/t - Скоросит цели = Пройденное расстояние / время прохождения
    }

    //Задержка на выстрел
    IEnumerator AllowRotate() {
        yield return new WaitForSeconds(0.4f);
        allowRotate = true;
    }

    protected virtual Vector3 FindFutureTargetPosition()
    {
        //Позиция по-умолчанию
        targetPosition = target.transform.position;

        //Высчитываем точку, перед целью, по которой нужно произвести выстрел, чтобы попасть по движущейся цели

        float distance = (m_shootPoint.position - targetPosition).magnitude;
        float timeToTarget = distance / m_projectilePrefab.GetComponent<Projectile>().m_speed;  //t=S/V - Время до цели = Расстояние до цели / на скорость снаряда
        targetPosition = target.transform.position + targetSpeed * timeToTarget;                // Позиция с учетом скорости передвижения цели и полёта пули = текущая позиция + (S=V*t - Цель успеет пройти расстояние=Скорость цели*время до цели)
        
        return targetPosition;
    }

    IEnumerator Rotate() {

        while (true)
        {
            if (TargetCheck(target) && allowRotate)
            {
                //Поворот 
                targetPosition = FindFutureTargetPosition();                                                        //Предполагаемая точка атаки
                Vector3 directionToTarget = targetPosition - transform.position;                                    //Вектор до точки
                directionToTarget.y = 0;                                                                            //Поворот только в горизонтальной плоскости
                rotateQuaternion = Quaternion.LookRotation(directionToTarget);                                      //Поворот до точки
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, rotateQuaternion, 1f);  //Поворачиваем


                //Наведение пушки                
                float direction = Vector3.Distance(targetPosition, m_cannon.position);                              //Точное расстояние, приходится использовать Vector3.Distance
                float angle = Vector3.Angle(targetPosition - m_cannon.position, Vector3.up);                        //Расчетный угол наклона

                Vector3 tilt;                                                                                       //Вектор наклона по y

                if (angle > 100) {                                                                                  //Если расчетный вектор наклона больше 105 - нельзя опускать вниз ниже
                    angle = 100f;
                    tilt = new Vector3(0, Quaternion.AngleAxis(-angle, Vector3.up).y, 0);                           //Рассчитываем уклон исходя из максимально допустимого                    
                }
                else if (angle < 70) {                                                                              //Если расчетный вектор наклона меньше 70 - нельзя поднимать вверх выше
                    angle = 70f;
                    tilt = new Vector3(0, Quaternion.AngleAxis(angle, Vector3.up).y, 0);                            //Рассчитываем подъем исходя из максимально допустимого
                }
                else
                    tilt = new Vector3(0, targetPosition.y, 0) - new Vector3(0, m_cannon.position.y, 0);            //Рассчитываем подъем исходя из вектора между пушкой и целью                

                Vector3 tiltDirectionToTarget = new Vector3(transform.forward.x, 0, transform.forward.z)            //Вектор наклона базируется на нормали текущего поворота башни в горизонтальной плоскости
                    * direction                                                                                     //Нормальный вектор с помощью дистацинии превращается в полный вектор
                    + tilt                                                                                          //Добавляем наклон
                    ;

                Quaternion tiltQuaternion = Quaternion.LookRotation(tiltDirectionToTarget);                         //Поворот (наклон) до точки
                m_cannon.rotation = Quaternion.RotateTowards(m_cannon.rotation, tiltQuaternion, 1f);                //Поворачиваем (наклоняем)

                ////Перемещаем точку выстрела, которая в конце ствола следом за стволом по оси y
                //float y = m_shootPointY;
                //if (angle > 90)
                //    y -= (m_shootPoint.localPosition.z * Mathf.Tan(angle - 90));
                //else if (angle < 90)
                //    y += (m_shootPoint.localPosition.z * Mathf.Tan(90 - angle));
                //m_shootPoint.localPosition = new Vector3(m_shootPoint.localPosition.x, y, m_shootPoint.localPosition.z);

                //m_moveTarget.transform.position - m_shootPoint.position;

            }
            yield return null;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, m_range);
        if (target !=null)
        {
            //Направление выстрела
            Debug.DrawRay(m_shootPoint.position, m_cannon.forward * m_range, Color.green);
            //Направление от точки выстреа к цели - при совпадение 1 и 2 в вертикальной плоскости можно стрелять
            Debug.DrawRay(m_shootPoint.position, (targetPosition - m_shootPoint.position), Color.red);            
        }
    }
}
