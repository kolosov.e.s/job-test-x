﻿public class CannonProjectile : Projectile
{
    //Добавили уничтожение за пределами камеры
    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
