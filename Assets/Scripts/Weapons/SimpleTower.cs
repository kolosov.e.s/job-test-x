﻿using UnityEngine;

public class SimpleTower : Tower
{
    //Переопределяем выстрел
    protected override void Shot(GameObject target) {
        var projectile = Instantiate(m_projectilePrefab, transform.position + Vector3.up * 1.5f, Quaternion.identity) as GameObject;
        projectile.GetComponent<GuidedProjectile>().m_target = target.gameObject;
    }
}
