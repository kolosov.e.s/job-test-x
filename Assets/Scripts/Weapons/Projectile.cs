﻿using UnityEngine;

public abstract class Projectile : MonoBehaviour {


    public float m_speed = 10f;
    public int m_damage = 10;
    //Движение снаряда по прямой независимо от FPS - можно переопределить уже в конкретном классе снаряда
    protected virtual void Update ()
    {
        transform.position += transform.forward * m_speed * Time.deltaTime;
    }
    // Стандартный обработчик попадания в цель - можно переопределить при необходимости
    protected virtual void OnTriggerEnter(Collider other) {
        var monster = other.gameObject.GetComponent<Monster>();
        if (monster == null)
            return;

        monster.TakeDamage(m_damage);
        Destroy(gameObject);
    }
}
