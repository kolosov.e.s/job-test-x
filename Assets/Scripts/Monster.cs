﻿using UnityEngine;

public class Monster : MonoBehaviour {

	public GameObject m_moveTarget;
	public float m_speed = 5f;
	public int m_maxHP = 30;
	const float m_reachDistance = 0.3f;

    public int m_hp;

	void Start() {        
        m_hp = m_maxHP;
	}

	void Update () {
		if (m_moveTarget == null)
			return;

        //Проверка расстояний через квадраты быстрее, чем Vector3.Distance
        if ((m_moveTarget.transform.position - transform.position).sqrMagnitude <= m_reachDistance)
        {
            Die();
            return;
		}
        //Движение к точке со скорость, не зависящей от FPS
		var translation = m_moveTarget.transform.position - transform.position;	        //Направление перемещения к точке	
        translation = translation.normalized * m_speed * Time.deltaTime;                //Применяем скорость и deltaTime
        transform.position += translation;                                              //Перемещаем к точке
    }
    //Получаем дамаг
    public void TakeDamage(int damage) {
        m_hp -= damage;
        if (m_hp <= 0) {
            Die();
        }
    }
    //Умираем и удаляемся из списка живых
    private void Die() {
        Spawner.instance.livingMonsters.Remove(gameObject);
        Destroy(gameObject);
    }
}
