﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {
	public float m_interval = 3;
	public GameObject m_moveTarget;

    [HideInInspector]
    public List<GameObject> livingMonsters = new List<GameObject>();

    //Создали статичный инстанс для удобства доступа к списку монстров
    private static Spawner _instance { get; set; }    
    public static Spawner instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Spawner>();
            return _instance;
        }
    }
    //Отвязываемся от расчета времени в Update и переходим на более стабильный Coroutine
    void Start () {        
        StartCoroutine(Spawn());
	}

    IEnumerator Spawn() {
        while (true)
        {
            var newMonster = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            newMonster.AddComponent<Rigidbody>().useGravity = false;
            newMonster.transform.position = transform.position;
            newMonster.AddComponent<Monster>().m_moveTarget = m_moveTarget;
            newMonster.transform.localScale = new Vector3(1f, 2f, 1f); //Подрастил монстра, чтобы пушка попадала ;)            
            //Поместили монстра в список живых
            livingMonsters.Add(newMonster);

            yield return new WaitForSeconds(m_interval);
        }
    }
}
